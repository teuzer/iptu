// ==UserScript==
// @name         Luziania
// @namespace    Luziania
// @version      1.9
// @downloadURL  https://bitbucket.org/teuzer/iptu/raw/master/luziania.user.js
// @updateURL    https://bitbucket.org/teuzer/iptu/raw/master/luziania.user.js
// @description  IPTU LUZIANIA
// @author       Teuzer
// @match        http://gestaoluziania.com.br/sig/*
// @grant        none
// ==/UserScript==
$(function() {
    let machine = 1;
    if (localStorage.machine) {
        machine = Number(localStorage.machine);
    }else{
        localStorage.machine = 1;
    }
    //'use strict';http://trindade.coretecnologia.net.br/sig/app.html

    /*$("label:contains('C�digo')").eq(0).next().val(1);
    $("button:contains('Pesquisar')").trigger("click");
    if($("button[title*='Exibir detalhes']").length == 1){
        $("button[title*='Exibir detalhes']").trigger("click");
    }*/
    // Your code here...
    $('head').append('<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">');
    $("body")
        .append('<div id="myneup" style="position: absolute;top: 105px;float: right;right:200px;width:170px;padding:10px;text-align:center;z-index:9999999;background:#0088cc;border:0.5px solid #204d74;color: white;border-radius: 5px;font-size: 11pt;cursor:pointer;margin-left:5px;"><span id="mynuvemup"><i class="fas fa-redo-alt" style="float: left;font-size: 16pt;"></i></span><span id="mysyncup" style="display:none"><i class="fas fa-sync-alt fa-spin" style="float: left;font-size: 16pt;"></i></span>Iniciar Pesquisa</div>')
    let instanse = false
    let timetoTryAgain = 14000;
    let rangeLimite = 15000;
    let lowerLimit = (machine - 1) * rangeLimite;
    let upLimit = machine * rangeLimite;
    if (localStorage.clickcount) {
        if (Number(localStorage.clickcount) >= upLimit){
            localStorage.clickcount = lowerLimit;
        }else if (Number(localStorage.clickcount) < lowerLimit){
            localStorage.clickcount = lowerLimit;
        }else{
            localStorage.clickcount = Number(localStorage.clickcount) - 2;
        }
    } else {
        localStorage.clickcount = lowerLimit;
    }
    function nextRecord(curValue){
        let AlreadyTaken = JSON.parse(localStorage.getItem("AlreadyTaken"));
        if(!AlreadyTaken.includes(curValue)){
            //console.log("foi: "+Number(localStorage.clickcount));
            if (Number(localStorage.clickcount) >= upLimit){
                alert("FINAL");
            }else{
                $("#myneup").trigger("click");
            }
        }else{
            //console.log("nao: "+curValue);
            localStorage.clickcount = curValue + 1;
            nextRecord(Number(localStorage.clickcount));
        }
    }
    $("#myneup").on("click",function(){

            if(!instanse){
                instanse = true;
                let finalizadas;
                $("#mynuvemup").hide();
                $("#mysyncup").show();
                $.ajax({
                    method: "POST",
                    url: 'http://gestaoluziania.com.br/sig/rest/imovelController/pesquisarImoveis',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'x-auth-token': JSON.parse(localStorage.getItem('sigProdata.24')).token
                    },
                    data: JSON.stringify({
                        cci: localStorage.clickcount,
                        descricaoModuloAtual: "servicosonline",
                        isConsultaText: false,
                        moduloAtual: "24",
                        nomeTelaAtualAutocomplete: null,
                        propriedadeDescricao: "cci",
                        propriedadeValor: "cci",
                        tabela: {},
                        tipo_certidao: 1,
                        tipo_consulta: 1
                    }),
                    dataType: 'application/json;charset=UTF-8',
                    complete:function(response,status){
                        if(response.status != 200){
                            location.reload();
                        }
                        let info = JSON.parse(response.responseText);
                        if(info.length == 1){
                            info = info[0];
                            $.ajax({
                                method: "POST",
                                url:"https://script.google.com/macros/s/AKfycbxGYwc5XGAScvic3kIDtRm2Qd9-x66xOUXVkWp18C_zB4hdTmFnKFZc/exec",
                                data:JSON.stringify({
                                    cidade : "luziania",
                                    cadastro : info.cci,
                                    setor : info.bairro,
                                    rua : info.logradouro,
                                    quadra : info.quadra1,
                                    lote : info.lote1,
                                    inscricao : info.inscricao,
                                    cpf: info.cgc,
                                    nome:info.nome
                                }),
                                dataType: 'application/json',
                                complete:function(data){
                                    instanse = false;
                                    $("#mynuvemup").show();
                                    $("#mysyncup").hide();
                                    let AlreadyTaken = JSON.parse(localStorage.getItem("AlreadyTaken"));
                                    AlreadyTaken.push(Number(localStorage.clickcount));
                                    localStorage.setItem("AlreadyTaken",JSON.stringify(AlreadyTaken));
                                    localStorage.clickcount = Number(localStorage.clickcount) + 1;
                                    setTimeout(function(){
                                        $("#myneup").trigger("click");
                                    },timetoTryAgain);
                                }
                            });
                        }else{
                            instanse = false;
                            $("#mynuvemup").show();
                            $("#mysyncup").hide();
                            let AlreadyTaken = JSON.parse(localStorage.getItem("AlreadyTaken"));
                            AlreadyTaken.push(Number(localStorage.clickcount));
                            localStorage.setItem("AlreadyTaken",JSON.stringify(AlreadyTaken));
                            localStorage.clickcount = Number(localStorage.clickcount) + 1;
                            setTimeout(function(){
                                  $("#myneup").trigger("click");
                            },timetoTryAgain);
                        }
                    }
                });
            }
      });
    setTimeout(function(){
        nextRecord(Number(localStorage.clickcount));
    },timetoTryAgain);
});