// ==UserScript==
// @name         opener
// @namespace    opener
// @version      1.0
// @downloadURL  https://bitbucket.org/teuzer/iptu/raw/master/opener.user.js
// @updateURL    https://bitbucket.org/teuzer/iptu/raw/master/opener.user.js
// @description  opener a partir da pagina do google
// @author       Teuzer
// @match        https://www.google.com*
// @grant        GM_xmlhttpRequest
// @grant        GM_openInTab
// ==/UserScript==

(function() {
    let times = 0;
    let myonpen = false;
    let urlToOpenTab = "http://gestaoluziania.com.br/sig/app.html#/servicosonline/debito-imovel";
    let totalTime = 4;
    let timetoOpenNewTab = 3 * 1000;
    let timetoCloseTab = 20 * 60 * 1000;
    let timetoReloadCurTab = 6 * 1000;
    let createOpen = function(){
        myonpen = GM_openInTab(urlToOpenTab, {loadInBackground :true});
        setTimeout(function(){
            destroyOpen();
        },timetoCloseTab);
    };
    let destroyOpen = function(){
        myonpen.close();
        myonpen = false;
        times = times+1;
        if(times == totalTime){
            setTimeout(function(){
                window.location.reload();
            },timetoReloadCurTab);
        }else{
            setTimeout(function(){
                createOpen();
            },timetoOpenNewTab);
        }
    };
    createOpen();
})();