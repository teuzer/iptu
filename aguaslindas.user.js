// ==UserScript==
// @name         AguasLindas
// @namespace    http://tampermonkey.net/
// @version      0.1
// @downloadURL  https://bitbucket.org/teuzer/iptu/raw/master/aguaslindas.user.js
// @updateURL    https://bitbucket.org/teuzer/iptu/raw/master/aguaslindas.user.js
// @description  IPTU AGUAS LINDAS
// @author       Teuzer
// @match        http://sistema.aguaslindasdegoias.go.gov.br/sig/*
// @grant        none
// ==/UserScript==
$(function() {
    //'use strict';http://trindade.coretecnologia.net.br/sig/app.html

    /*$("label:contains('Código')").eq(0).next().val(1);
    $("button:contains('Pesquisar')").trigger("click");
    if($("button[title*='Exibir detalhes']").length == 1){
        $("button[title*='Exibir detalhes']").trigger("click");
    }*/
    // Your code here...
    $('head').append('<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">');
    $("body")
        .append('<div id="myneup" style="position: absolute;top: 105px;float: right;right:200px;width:170px;padding:10px;text-align:center;z-index:9999999;background:#0088cc;border:0.5px solid #204d74;color: white;border-radius: 5px;font-size: 11pt;cursor:pointer;margin-left:5px;"><span id="mynuvemup"><i class="fas fa-redo-alt" style="float: left;font-size: 16pt;"></i></span><span id="mysyncup" style="display:none"><i class="fas fa-sync-alt fa-spin" style="float: left;font-size: 16pt;"></i></span>Iniciar Pesquisa</div>')
    let instanse = false
    if (localStorage.clickcount) {

    } else {
        localStorage.clickcount = 1;
    }
    $("#myneup").on("click",function(){

            if(!instanse){
                instanse = true;
                let finalizadas;
                $("#mynuvemup").hide();
                $("#mysyncup").show();
                $.ajax({
                    method: "POST",
                    url: 'http://sistema.aguaslindasdegoias.go.gov.br/sig/rest/imovelController/pesquisarImoveis',//'http://trindade.coretecnologia.net.br/sig/rest/imovelController/pesquisarImoveis',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'x-auth-token': JSON.parse(localStorage.getItem('sigProdata.24')).token
                    },
                    data: JSON.stringify({
                        cci: localStorage.clickcount,
                        descricaoModuloAtual: "servicosonline",
                        isConsultaText: false,
                        moduloAtual: "24",
                        nomeTelaAtualAutocomplete: null,
                        propriedadeDescricao: "cci",
                        propriedadeValor: "cci",
                        tabela: {},
                        tipo_certidao: 1,
                        tipo_consulta: 1
                    }),
                    dataType: 'application/json;charset=UTF-8',
                    complete:function(response,status){
                        if(response.status != 200){
                            location.reload();
                        }
                        let info = JSON.parse(response.responseText);
                        if(info.length == 1){
                            info = info[0];
                            $.ajax({
                                method: "POST",
                                url:"https://pantacreme.com/ajaxhidro.php",
                                data:{
                                    cidade : 'aguaslindas',
                                    cadastro : info.cci,
                                    setor : info.bairro,
                                    rua : info.logradouro,
                                    quadra : info.quadra1,
                                    lote : info.lote1,
                                    inscricao : info.inscricao,
                                    cpf: info.cgc,
                                    nome:info.nome
                                },
                                dataType: 'application/json',
                                complete:function(data){

                                    instanse = false;
                                    $("#mynuvemup").show();
                                    $("#mysyncup").hide();
                                    localStorage.clickcount = Number(localStorage.clickcount) + 1;
                                    setTimeout(function(){
                                        $("#myneup").trigger("click");
                                    }
                                    ,10000);
                                }
                            });
                        }else{
                            instanse = false;
                            $("#mynuvemup").show();
                            $("#mysyncup").hide();
                            localStorage.clickcount = Number(localStorage.clickcount) + 1;
                            setTimeout(function(){
                                  $("#myneup").trigger("click");
                            }
                            ,10000);
                        }
                    }
                });
            }
      });
    setTimeout(function(){
        $("#myneup").trigger("click");
    },10000);
});